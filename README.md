# 🚴 HugoVelo - Site statique dans un container Docker

![](screenshot.png)

<!-- TOC -->
* [🚴 HugoVelo - Site statique dans un container Docker](#-hugovelo---site-statique-dans-un-container-docker)
  * [🐳 Description du Dockerfile étape par étape](#-description-du-dockerfile-étape-par-étape)
  * [🛠️ Utilisation](#-utilisation)
  * [🌍Accès au site](#accès-au-site)
<!-- TOC -->

Ce repo contient le code source d'un site web statique généré à l'aide du framework [Hugo](https://gohugo.io/).

Le `Dockerfile` fourni permet de générer un site web depuis les fichiers Markdown du dossier `content`, puis de le servir
à l'aide d'un container Nginx. Comme précisé dans l'exercice, j'utilise un `Dockerfile` multistage afin de générer
l'image permettant de servir le site.

## 🐳 Description du Dockerfile étape par étape

1. 📦 Image de base

```Dockerfile
FROM alpine:latest as builder
```
Utilise la dernière version de l'image Alpine Linux comme base. De plus, on utilise le label `builder` pour être en mesure
de copier les fichiers issus du processus par la suite.

2. 🛠️ Définition du répertoire de travail

```Dockerfile
WORKDIR /app/
```
Définit le répertoire de travail à /app.

3. 🚀 Installation middlewares

```Dockerfile
RUN apk add --no-cache --repository=https://dl-cdn.alpinelinux.org/alpine/edge/community hugo && \
    apk add --no-cache git
```
Permet l'installation d'Hugo, et de Git permettant de récupérer le thème depuis le repo officiel.

4. 📂 Copie du site web

```Dockerfile
COPY . .
```
Copie les sources du site web vers le container

5. 🎨 Récupération du thème

```Dockerfile
RUN git submodule add --force https://github.com/theNewDynamic/gohugo-theme-ananke.git velohugo/themes/ananke
```
Cette étape permet d'alléger la taille du repo. On télécharge et intègre le thème pendant le build.

6. 🏗️ Définition du répertoire de travail à nouveau

```Dockerfile
WORKDIR /app/velohugo
```

7. 🌐 Construire le site web

```Dockerfile
RUN mkdir -p /app/velohugo/public && \
    hugo -d /app/velohugo/public
```
On crée un répertoire `public` où le site web statique sera généré.

8. 📦 Image de base pour le serveur web

```Dockerfile
FROM nginx:alpine
```
La version `alpine` de `nginx` est légère et permet de servir le site web de façon simple.

9. 📁 Définition du répertoire de travail dans le container Nginx

```Dockerfile
WORKDIR /usr/share/nginx/html
```

10. 🖼️ Copie du site depuis le stage `builder`

```Dockerfile
COPY --from=builder /app/velohugo/public /usr/share/nginx/html
```

11. Exposition du port

```Dockerfile
EXPOSE 80
```

Le site web sera accessible depuis le port 80.

## 🛠️ Utilisation

- En premier lieu, cloner le repo : 

```bash
git clone https://gitlab.com/sdv9972401/ci_cd/hugovelo-exercice-2.git
```

- Construire l'image Docker :

```bash
cd hugovelo-exercice2 && docker build -t fab/multistage .
```

- Lancer le container en exposant le port 80 :
```bash
docker run -p 80:80 -v fab/multistage
```

## 🌍Accès au site

Ouvrir un navigateur et accéder à http://localhost pour parcourir le site



